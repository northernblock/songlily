pragma solidity ^0.4.4;
import 'zeppelin-solidity/contracts/math/SafeMath.sol';
contract AudioTracks {
  using SafeMath for uint256;

  struct AudioTrack {
    address label;
    address[10] publishers;
    uint8 numPublishers;
    uint256 price;
    string metadata;
    uint256[11] revSplits;
  }

  uint256 public trackID = 1;
  mapping(uint256 => AudioTrack) public audioTracks;

  function AudioTracks() public {
    // constructor
  }

  function addAudioTrack(address label, uint8 numPublishers, address[10] publishers, uint256 price, uint256[11] revSplits, string metadata) public {

    require(price > 0);
    require(label != 0x0);
    uint256 splitTotal = 0;
    //require(revSplits.length == numPublishers + 1);
    for(uint8 i=0;i<numPublishers+1 ;i++) {
      splitTotal = splitTotal.add(revSplits[i]);
    }
    require(splitTotal == 100);
    AudioTrack memory track = AudioTrack({
      label: label,
      publishers: publishers,
      price: price,
      numPublishers: numPublishers,
      metadata: metadata,
      revSplits: revSplits
    });
    audioTracks[trackID] = track;
    trackID++;
  }

  function calculatePriceForTrack(uint256 _trackID, uint256 commission) public constant returns(uint256) {
    uint256 price = audioTracks[_trackID].price;
    if(price == 0) return 0;
    uint extraCommission = price.mul(commission).div(100);
    return price.add(extraCommission);
  }


  function getTrack(uint256 _trackID) public constant returns(address, uint8, address[10], uint256 price, uint256[11] revSplit) {
    return (audioTracks[_trackID].label, audioTracks[_trackID].numPublishers, audioTracks[_trackID].publishers , audioTracks[_trackID].price, audioTracks[_trackID].revSplits);
  }

}
