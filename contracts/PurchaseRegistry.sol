pragma solidity ^0.4.4;

import "./AudioTracks.sol";

contract PurchaseRegistry {

  using SafeMath for uint256;

  enum LicenseType { MasterLicense, CompositionLicense }

  struct Purchase {
    address purchasor;
    uint256 trackID;
    uint256 trackPrice;
    uint256 purchaseTime;
    uint256 commission;
    uint256 totalPrice;
    uint256[] licenses;
  }

  struct LicenseBundle {
    address licensor;
    address licensee;
    uint256 licenseStartTime;
    LicenseType licenseType;
    uint256 revSplit;
  }
  uint256 public purchaseID = 1;

  uint256 public licenseID = 1;
  uint256 public commission;
  address public commissionAddress;

  mapping(uint256 => Purchase) public purchases;
  mapping(uint256 => LicenseBundle) public licenseBundles;
  AudioTracks public audioTracks;

  function PurchaseRegistry(address _audioTrackContract, uint256 _commission, address _commissionAddress) public {
    audioTracks = AudioTracks(_audioTrackContract);
    commission = _commission;
    commissionAddress = _commissionAddress;
  }


  function purchaseTrack(uint256 trackID) payable public {
    uint totalPrice = audioTracks.calculatePriceForTrack(trackID, commission);
    if(totalPrice == 0) {
      revert();
    }

    require(totalPrice == msg.value);
    var (label, numPublishers, publishers, price, revSplits) = audioTracks.getTrack(trackID);
    uint256[] memory licenses = new uint[](numPublishers + 1);

    uint labelSplit = price.mul(revSplits[0]).div(100);
    label.transfer(labelSplit);
    licenseBundles[licenseID] = createLicenseBundle(msg.sender, label, LicenseType.MasterLicense, labelSplit);
    licenses[0] = licenseID;
    licenseID++;


    for(uint8 i=0;i<numPublishers; i++) {
      uint publisherSplit = price.mul(revSplits[i+1]).div(100);
      publishers[i].transfer(publisherSplit);
      LicenseBundle memory license = createLicenseBundle(msg.sender, publishers[i], LicenseType.CompositionLicense, publisherSplit);
      licenseBundles[licenseID] = license;
      licenses[i+1] = licenseID;
      licenseID++;
    }

    purchases[purchaseID] = Purchase({
      purchasor: msg.sender,
      trackID: trackID,
      trackPrice: price,
      purchaseTime: now,
      commission: commission,
      totalPrice: totalPrice,
      licenses: licenses
    });

    purchaseID++;
    uint256 commissionSplit = msg.value.sub(price);
    commissionAddress.transfer(commissionSplit);
  }

  function createLicenseBundle(address _licensee, address _licensor, LicenseType _licenseType, uint256 _revSplit) constant internal returns (LicenseBundle) {

    LicenseBundle memory _bundle = LicenseBundle({
      licensor: _licensor,
      licensee: _licensee,
      licenseType: _licenseType,
      licenseStartTime: now,
      revSplit: _revSplit
    });

    return _bundle;

  }

  function getLicense(uint256 _licenseID) public constant returns(address licensor,
    address licensee, uint licenseStartTime, uint Licensetype, uint256 revSplit) {
      LicenseBundle memory _bundle = licenseBundles[_licenseID];
      return (
        _bundle.licensor, _bundle.licensee, _bundle.licenseStartTime, uint(_bundle.licenseType) ,
        _bundle.revSplit
      );
  }

  function getLicenseIDs(uint256 _purchaseID) public constant returns(uint[]) {
    return purchases[_purchaseID].licenses;
  }

}
