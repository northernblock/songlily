var PurchaseRegistry = artifacts.require('./PurchaseRegistry')
var AudioTracks = artifacts.require('./AudioTracks')

contract('Purchase Registry', function(accounts) {
  var audioTracks;
  var purchaseRegistry;
  let commission = 20;
  let label = "0x92e52a1a235d9a103d970901066ce910aacefd37"
  let numPublishers = 1;
  let publishers = ["0x4ebc3c60f03c800d8435fd9c183c6e0fc07cc699"];
  let price = 2000000000000000000; //2 ETH
  let revSplit = [60, 40]
  let metadata = "metadata"
  beforeEach( async function () {
    audioTracks = await AudioTracks.new();
    await audioTracks.addAudioTrack(label, numPublishers, publishers, price, revSplit, metadata);

    let commission_address = accounts[4]

    purchaseRegistry = await PurchaseRegistry.new(audioTracks.address, commission, commission_address);


  });

  it("should buy an audio track", async function() {

    let trackID = 1
    let price = await audioTracks.calculatePriceForTrack(trackID, commission);
    let tx = await purchaseRegistry.purchaseTrack(trackID, { value: price })
    let purchase = await purchaseRegistry.purchases(1);
    let labelBalance = web3.eth.getBalance(label)
    let publisherBalance = web3.eth.getBalance(publishers[0])
    let licenses = await purchaseRegistry.getLicenseIDs(1);
  });

});
