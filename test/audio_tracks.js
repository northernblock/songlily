var AudioTracks = artifacts.require('./AudioTracks')

contract('audioTracks.js', function(accounts) {
  var audioTracks;
  beforeEach( async function () {
    audioTracks = await AudioTracks.new();
  });

  it("should add an audio track", async function() {
    let label = accounts[0]
    let numPublishers = 1;
    let publishers = [accounts[1]];
    let price = 1;
    let revSplit = [50, 50]
    let metadata = "metadata"
    await audioTracks.addAudioTrack(label, numPublishers, publishers, price, revSplit, metadata);
  });

  it('should get track data', async function () {
    let label = accounts[0]
    let numPublishers = 1;
    let publishers = [accounts[1]];
    let price = 1;
    let revSplit = [50, 50]
    let metadata = "metadata"
    await audioTracks.addAudioTrack(label, numPublishers, publishers, price, revSplit, metadata);
    let track = await audioTracks.getTrack(1);
    assert(track[0] == label);
    assert(track[1] == numPublishers);
    assert(track[2][0] == publishers[0]);
    assert(track[3] == price);
    assert(track[4][0] == revSplit[0]);
    assert(track[4][1] == revSplit[1]);
  });

  it('should calculate the price of the track with the commission', async () => {
    let trackID = 1;
    let label = accounts[0]
    let numPublishers = 1;
    let publishers = [accounts[1]];
    let price = 2000000000000000000;
    let revSplit = [50, 50]
    let metadata = "metadata"
    let commission = 20;
    await audioTracks.addAudioTrack(label, numPublishers, publishers, price, revSplit, metadata);
    let returnedPrice = await audioTracks.calculatePriceForTrack(trackID, commission);
    let expectedPrice = 2400000000000000000
    assert(returnedPrice.toString() == expectedPrice.toString());
  })


});
