var AudioTracks = artifacts.require("./AudioTracks.sol");
var PurchaseRegistry = artifacts.require("./PurchaseRegistry.sol");
var utils = require('./../deployment_utils')


//
module.exports = async function(deployer) {

  var args = [];
  var params = [web3, AudioTracks.bytecode, AudioTracks.abi].concat(args);
  var gas = await utils.getSuggestedGasForContract.apply(utils, params);
  var deployArgs = [AudioTracks].concat(args);
  deployArgs.push({ gas: gas });

  deployer.deploy.apply(deployer, deployArgs).then(async () => {
    var args = [
      AudioTracks.address,
      20,
      "0xc4197FFFF03Fbae5aB3DfFe0DA474Fc90311A2db"
    ];
    var params = [web3, PurchaseRegistry.bytecode, PurchaseRegistry.abi].concat(args);
    var gas = await utils.getSuggestedGasForContract.apply(utils, params);
    var deployArgs = [PurchaseRegistry].concat(args);
    deployArgs.push({ gas: gas });
    deployer.deploy.apply(deployer, deployArgs).then(async () => {
      console.log('Done');
    });

  });
  // deployer.deploy(AudioTracks).then(() => {
  //   var commission = 20;
  //   var commissionAddress = "0xc4197FFFF03Fbae5aB3DfFe0DA474Fc90311A2db";
  //   return deployer.deploy(PurchaseRegistry, AudioTracks.address, commission, commissionAddress)
  // }).then(() => {
  //   console.log("Done");
  // });
};
